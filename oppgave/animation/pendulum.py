# Animates the pendulum

import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np

# SETUP
########################################################################

dt  = 1/15                  # steps
t_0 = (np.pi)/4             # start angle
p_0 = 0                     # start speed
T   = 20                    # virtual runtime (sec)
L   = 1.02                  # length of line
N   = int(T / dt+1)         # loop length
t   = np.linspace(0, T, N)  # from 0 to T, N steps

# initialize the calculation array for theta and phi, make all initial
# values = 0, with size of T
theta = np.zeros(np.shape(t))
phi   = np.zeros(np.shape(t))

# make zero values of both phi and theta equal initial given values
theta[0] = p_0
phi[0]   = t_0

########################################################################
# /SETUP

# fill theta and phi arrays
for n in range(N - 1):
    theta[n+1] = (theta[n] + dt * phi[n])/(1 + dt**2)
    phi[n+1]   = phi[n] - ((dt * theta[n] + dt**2 * phi[n])/(1 + dt**2))

# prepare animation
fig = plt.figure()
ax = plt.axes(xlim=(-1, 1), ylim=(-1.5, 0))
line, = ax.plot([], [])
sphere = plt.Circle(([], []), .07)

def init():
    line.set_data([], [])
    sphere.center = (-1, 0)
    ax.add_patch(sphere)
    return line, sphere,

def animate(i):
    x = L*np.sin(theta[i])
    y = -L*np.cos(theta[i])
    line.set_data([0, x], [0, y])
    sphere.center = (x, y)
    return line, sphere,

anim = animation.FuncAnimation(fig, animate,
                                 init_func=init,
                                 frames=200,
                                 interval=40,
                                 blit=True)

#anim.save('pendulum_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])

plt.show()
